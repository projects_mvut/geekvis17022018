var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();
redis.subscribe('star-channel', function(err, count) {
});
redis.on('message', function(channel, message) {
    console.log('Message Recieved: ' + message);
    message = JSON.parse(message);
    io.emit('star-channel-' + message.data.data.user, message.data.data.star);
});
http.listen(3000, function(){
    console.log('Listening on Port 3000');
});
