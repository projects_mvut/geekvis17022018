<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class PostTableSeeder extends Seeder
{

	public function image($q,$w){
		$faker = Faker::create();
		$image=$faker->image('./public/uploads/seed',$q, $w);
		return explode("public", $image)[1];
	}

	public function run()
	{
		$faker = Faker::create();
		foreach (range(1,20) as $index) {
			$name=$faker->name;
			DB::table('users')->insert([
				'name' => $name,
				'bio' => $faker->sentence,
				'username'=>str_slug($name),
				'imgUrl'=>$this->image(200,200),
				'email' => $faker->email,
				'password' => bcrypt('secret'),
				'editor'   => $faker->randomElement([1,0,0,0]),
				]);
		}
		foreach (range(1,6) as $index) {
			DB::table('categories')->insert([
				'title'=>$faker->sentence(3),
				]);
		}
		foreach (range(1,40) as $index) {
			DB::table('arrows')->insert([
				'user_id'=>$faker->numberBetween(1,10),
				'head_id'=>$faker->numberBetween(11,20),
				]);
		}
		foreach (range(1,40) as $index) {
			$title=$faker->sentence;
			$a=$faker->randomElement([0,1]);
			if($a){
				DB::table('posts')->insert([
					'title'=>$title,
					'slug'=>str_slug($title),
					'type'=>$faker->randomElement(['article','quiz','question']),
					'content'=>$faker->text,
					'user_id'=>$faker->numberBetween(1,15),
					'category_id'=>$faker->numberBetween(1,6)
					]);
			}
			else{
				DB::table('posts')->insert([
					'title'=>$title,
					'slug'=>str_slug($title),
					'type'=>$faker->randomElement(['article','quiz','question']),
					'content'=>$faker->text,
					'user_id'=>$faker->numberBetween(1,15),
					'category_id'=>$faker->numberBetween(1,6),
					'imgUrl'=>$this->image(480,320)
					]);
			}
		}

		foreach (range(1,60) as $index) {
			$a=$faker->randomElement([0,0,1]);
			if($a){
				DB::table('comments')->insert([
					'content'=>$faker->text,
					'user_id'=>$faker->numberBetween(1,15),
					'post_id'=>$faker->numberBetween(1,30)
					]);
			}
			else{
				DB::table('comments')->insert([
					'content'=>$faker->text,
					'user_id'=>$faker->numberBetween(1,15),
					'post_id'=>$faker->numberBetween(1,30),
					'imgUrl'=>$this->image(480,320)
					]);
			}
		}
	}
}
