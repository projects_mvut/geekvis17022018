<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UserTableSeeder extends Seeder
{
	public function image(){
		$faker = Faker::create();
		$image=$faker->image('./public/uploads/seed',200, 200);
		return explode("public", $image)[1];
	}

	public function run()
	{
		$faker = Faker::create();
		DB::table('users')->insert([
			'name'   => 'Bharat Garg',
			'bio' => $faker->sentence,
			'username'   => 'bharat',
			'imgUrl'=>$this->image(),
			'email'      => 'bharatbgarg4@gmail.com',
			'password'   => bcrypt('qwert'),
			'admin'   => true,
			'editor'   => true,
			]);
		DB::table('users')->insert([
			'name'   => 'Himanshu Vasistha',
			'bio' => $faker->sentence,
			'username'   => 'himanshu',
			'imgUrl'=>$this->image(),
			'email'      => 'himanshu.vasistha@gmail.com',
			'password'   => bcrypt('qwert'),                
			'admin'   => true,
			]);
	}
}
