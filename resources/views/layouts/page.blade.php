@extends('main')

@section('maincontent')
<div class="spacer">
</div>
    <div class="container">
    <div class="row">

      @yield('pagecontent')
      </div>
    </div>
   
@stop