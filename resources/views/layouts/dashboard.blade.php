@extends('layouts.pageFluid')

@section('pagecontent')
<div class="spacer"></div>
<div class="dashboard">
	<div class="col col-sm-3 sidebar">
		@if(Auth::check())
		@if(Auth::user()->admin)
		<ul class="list-group">
			<li class="list-group-item">
				<span class="badge" style="padding: 8px;border-radius: 5px;">
					<i class="fa fa-user-secret"></i> {{$count['admins']}}
				</span>
				<a href="/dashboard/admins">Admins</a>
			</li>
			<li class="list-group-item">
				<span class="badge" style="padding: 8px;border-radius: 5px;">
					<i class="fa fa-smile-o"></i> {{$count['editors']}}
				</span>
				<a href="/dashboard/editors">Editors</a>
			</li>
			<li class="list-group-item">
				<span class="badge" style="padding: 8px;border-radius: 5px;">
					<i class="fa fa-users"></i> {{$count['users']}}
				</span>
				<a href="/dashboard/users">Users</a>
			</li>
			<li class="list-group-item">
				<a href="/dashboard/user/create"><i class="fa fa-plus"></i> User</a>
			</li>
			<li class="list-group-item">
				<a href="/dashboard/email"><i class="fa fa-envelope"></i> Email</a>
			</li>
			<li class="list-group-item">
				<a href="/dashboard/ads">Ads</a>
			</li>
		</ul>
		@endif
		<ul class="list-group">
			<li class="list-group-item">
				<a href="/dashboard/{{Auth::user()->username}}/profile">Profile</a>
			</li>
			<li class="list-group-item">
				<a href="/dashboard/images">Images</a>
			</li>
		</ul>
		@endif
	</div>
	<div class="col col-sm-9">
	<div class="postbox" style="padding:20px;">
		@yield('dashboardcontent')
		</div>
	</div>
</div>
<div class="spacer"></div>

@stop