@extends('layouts.dashboard')

@section('dashboardcontent')
<h3>Select Posts</h3>
{!!Form::open(['url'=>'dashboard/email'])!!}
<div class="form-group hidden">
	{!! Form::text('posts', null, ['id'=>'storash']) !!}
</div>

<div class="form-group">
	<button type="submit" class="btn btn-success" name="admins">Mail to Admins</button>
	<button type="submit" class="btn btn-danger" name="all">Mail to All Users</button>
</div>
{!! Form::close() !!}

<div id="emal" class="row">
	@foreach($posts as $post)
	<div class="col col-sm-4">
	<div class="emailSel">
		<p class="hidden rocko" id="{{$post->id}}"></p>
		<h5>{{$post->title}}</h5>
		<div class="date">
			<i class="fa fa-clock-o"></i> Asked {{$post->created_at->diffForHumans()}} <br>
			<span class="views"> <i class="fa fa-eye"></i> {{$post->views}} Views</span>
		</div>
		@if($post->category)
		<span class="cat"> <i class="fa fa-bars"></i>
			{{$post->category->title}}
		</span>
		@else
		<span class="cat"> <i class="fa fa-bars"></i>
		</span>
		@endif
		<br>
		<span><i class="fa fa-thumbs-o-up"></i> {{$post->likes->count()}}</span>
		<span><i class="fa fa-comments-o"></i> {{$post->comments->count()}} </span>
	</div>
	</div>
	@endforeach
</div>


@stop