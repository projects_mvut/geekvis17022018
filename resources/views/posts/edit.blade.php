@extends('layouts.page')

@section('headinclude')
@include('partials.forms.tinymce')
@stop

@section('pagecontent')
<section>
	<div class="container">
		<div class="">
			<div class="outer-wrapper"  style="padding:10px 0;">
				<div class="col-md-12">
					<div class="heading-bar">
						<h3>Edit the Post</h3>
					</div>
				</div>
{!!Form::model($post,['url'=>'post/'.$post->slug,'method'=>'patch','files' => true,'class'=>'form-horizontal'])!!}

@include('partials.forms.post')

</div>
</div>
</div>
</section>
@stop