@extends('layouts.pageFluid')
@section('headinclude')
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
 @if(Auth::check())
 @if(Auth::user()->admin)
@include('partials.forms.tinymce')
@endif
@endif
@stop
@section('pagecontent')
<div class="spacer hidden-xs"></div>
						<div class="visible-xs" style="position: fixed;bottom: 0; z-index: 1; width: 100%;">
						<div class="text-center">
						<a href="#" class="btnCreatePost" data-toggle="modal"
data-target="#fsModal" style="width: 100%;display: block; background-color: #ffc5b5;"><i class="fa fa-plus"></i> Select Category</a>
						</div>
						</div>
<div class="col-md-3 col-sm-3 col-lg-3 hidden-xs" id="secondary">
	  		<ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="85" data-offset-bottom="285" style="background:white">

	  		  <li style="background-color:#fe8b6c"><a href="#" style="color:#fff;"><span class="glyphicon glyphicon-chevron-right"></span> <b>Categories</b></a></li>

  @foreach($cats as $cat)
@if(($cat->title) == ($post->category->title))

  <li class="active"><a href="/category/{{$cat->slug}}"><span class="glyphicon glyphicon-chevron-right"></span> {{$cat->title}}
</a></li>
@else
  <li><a href="/category/{{$cat->slug}}"><span class="glyphicon glyphicon-chevron-right"></span> {{$cat->title}}
</a></li>
@endif
  @endforeach

</ul>
























</div>
<div class="col-sm-6 wall">	
<div class="row"> <!-- added here -->
	


<div id="post" class="postbox bottom-margin-xs" style="padding-top:0; padding-bottom:0;">


<span class="gv-post-image">

				@if($post->imgUrl)
								@if($post->type=='question')
								<img src="{{$post->imgUrl}}" class="postimg postModalImage" style="object-position: top;" width="100%" data-post="{{$post->slug}}">
								<div class="modalQuestion modalQuestionHolder-{{$post->slug}}">
									<img src="{{$post->imgUrl}}" alt="" class="img-responsive" width="100%">


								</div>
								@else
								<img src="{{$post->imgUrl}}" class="postimg img-responsive" width="100%">
								@endif
								@endif

				</span>

		<span class="controls" style="position: absolute;top: 0;z-index:1; background:#00000045;right:0;">


										<a href="javascript:window.print()"><img src="/images/printer.svg" width="17"></a>
										
										<a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->twitter()}}" target="_blanks" class="twitter"><img src="/images/twitter.svg" width="17"></a>
										
										<a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->facebook()}}" target="_blank" class="facebook"><img src="/images/facebook.svg" width="17"></a>
									
										<a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->gplus()}}" target="_blank" class="google"><img src="/images/google-plus.svg" width="17"></a>



		<a href="#" data-container="body" data-toggle="popover" data-placement="bottom" data-content="{{$post->created_at->diffForHumans()}}"><i class="fas fa-info-circle"></i></a>


			@if($owner)
			<a href="/post/{{$post->slug}}/edit" class=""><i class="fas fa-pencil-alt"></i></a>
			<a href="#" class="" data-toggle="modal" data-target="#delete-{{$post->slug}}"><i class="far fa-trash-alt"></i></a>
					@endif

		</span>
		
		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="gv-post-title">
				<div class="cl" style="min-width: 50px;max-width: 50px; padding-left: 0;">
					<a href="/profile/{{$post->user->username}}">
						<img src="{{$post->user->imgUrl}}" alt="" class="img-circle
						"  width="100%">
					</a>


				</div>

				<div class="cl" style="line-height:0px;">
					<a href="/post/{{$post->slug}}" class="title"><h3>{{$post->title}}</h3><span style="font-size:10px;">	by: {{$post->user->name}}
</span></a>
				</div>
			</div>




			<span class="gv-post-info hidden">


				<div class="col-sm-12">
					<div class="row">
						<span class="info-blk"> 
							{{$post->user->name}}
						</span>
						

						<span class="info-blk"> 

							@if($post->category)
							@if($post->category->slug)
							<a href="/category/{{$post->category->slug}}" class="cat">
								@else
								<a href="/category/{{$post->category->id}}" class="cat">
									@endif

									<span class="cat">
										{{$post->category->title}}
									</span>
								</a>
								@else
								<a href="#" class="cat">
									<span class="cat">
									</span>
								</a>
								@endif
							</span>
							<span class="info-blk"> 

								Posted: <span>{{$post->created_at->diffForHumans()}}</span>
							</span>
							<span class="info-blk" style="padding:0"> 
								<div class="dropdown">
									<a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="display: block;padding:10px;">
										<img src="/images/share.svg" width="16">
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu social" aria-labelledby="dropdownMenu1">
										<li> <a href="javascript:window.print()"><img src="/images/printer.svg" width="17"> Print</a>  </li>
										<li> <a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->twitter()}}" target="_blanks" class="twitter"><img src="/images/twitter.svg" width="17"> Share on Twitter</a></li>
										<li><a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->facebook()}}" target="_blank" class="facebook"><img src="/images/facebook.svg" width="17"> Share on Facebook</a></li>
										<li><a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->gplus()}}" target="_blank" class="google"><img src="/images/google-plus.svg" width="17"></i> Share on Google+</a></li>
									</ul>
								</div>
							</span>
						</div>
					</span>



			<span class="gv-post-info hidden">
				<div class="col-sm-12">
				<div class="row">
				<a href="/profile/{{$post->user->username}}" class="user">
					<img src="{{$post->user->imgUrl}}" alt=""  class="img-circle" width="30">
				</a> {{$post->user->name}} &nbsp;|&nbsp;
								@if($post->category)
								@if($post->category->slug)
								<a href="/category/{{$post->category->slug}}" class="cat">
									@else
									<a href="/category/{{$post->category->id}}" class="cat">
										@endif

										<span class="cat">
											{{$post->category->title}}
										</span>
									</a>
									@else
									<a href="#" class="cat">
										<span class="cat">
										</span>
									</a>
									@endif
									&nbsp;|&nbsp;Posted: <span>{{$post->created_at->diffForHumans()}}</span>&nbsp;|&nbsp;Views: <span>{{$post->views}}</span>
									&nbsp;|&nbsp;	
									Share: 
										<a href="javascript:window.print()"><img src="/images/printer.svg" width="17"></a>
										<a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->twitter()}}" target="_blanks" class="twitter"><img src="/images/twitter.svg" width="17"></a>
										<a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->facebook()}}" target="_blank" class="facebook"><img src="/images/facebook.svg" width="17"></a>
										<a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->gplus()}}" target="_blank" class="google"><img src="/images/google-plus.svg" width="17"></i></a>
									
								</div>
								</div>
				</span>
				</div>
				</div>
				
				<span class="gv-post-content">
					<p>
						{!! $post->content !!}
					</p>
				</span>



					<div class="col-sm-12">
						<div class="">
							<span class="gv-post-info">

								
								<span class="info-blk no-bg"> 
									Views: {{$post->views}}
								</span>
								<span class="info-blk no-bg"> 
									Comments {{$post->comments->count()}}

								</span>
								@if($liked)
								<a href="/post/{{$post->slug}}/unlike">
								<span class="info-blk no-bg pull-right"> 
									<span class="sat"><img src="/images/like.svg" width="15"></i> &nbsp; {{$post->likes->count()}}</span></span></a>








									@else
									<a href="/post/{{$post->slug}}/like">
																	<span class="info-blk no-bg pull-right"> 
																		<span class="sat"><img src="/images/unlike.svg" width="15"> &nbsp;{{$post->likes->count()}}</span></span></a>
									@endif	


							</span>

						</div>
					</div>
				




				<div class="intract col-sm-12 hidden text-left" style="padding: 0px 20px;">
					@if($liked)
					<a href="/post/{{$post->slug}}/unlike"><span class="sat"><i class="fa fa-thumbs-up"></i> {{$post->likes->count()}}</span></a>
					@else
					<a href="/post/{{$post->slug}}/like"><span class="sat"><i class="fa fa-thumbs-o-up"></i> Like {{$post->likes->count()}}</span></a>
					@endif
					<span class="sat"><i class="fa fa-comments-o"></i> {{$post->comments->count()}} </span>
				</div>

				<div class="clearfix"></div>
			<div class="col-sm-12 col-xs-12 com">
			<div class="col-sm-12">
	<div class="addComment">
		@if(Auth::check())
		{!!Form::open(['url'=>'/post/'.$post->slug.'/comment','files'=>true])!!}
		<div class="form-group">
			{!! Form::label('Add Comment') !!}
			{!! Form::textarea('content',null,['placeholder'=>'Enter Your Comment','class'=>'form-control','rows'=>'4']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('Image ( Optional )') !!}
			{!! Form::file('imgUrl', null,['value'=>'0 2px 2px 0 rgba(0, 0, 0, 0.07), 0 4px 5px 0 rgba(0, 0, 0, 0.07), 0 3px 1px -2px rgba(0, 0, 0, 0.12)Upload Image']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
		</div>
		{!!Form::close()!!}
		@else
		<a href="/login/next/{{$post->slug}}" class="btn btn-info">Login To Comment</a>
		@endif
	</div>
</div>
			<div class="infinite-scroll">

				@include('partials.comment')

			</div>

							{!! $comments->render() !!}

			</div>
			@include('partials.ad')

		</div>
	</div>
	
	
	</div> <!-- added here -->
	</div>
	<div class="col col-sm-3"	>
	<div class="fx">
	


@include('partials.ad')
<div class="spacer" style="line-height:18px">&nbsp;</div>
@include('partials.ad')
<div class="spacer" style="line-height:18px">&nbsp;</div>





</div>

	</div>
	<div class="col-sm-12 spacer"></div>









<div class="col-sm-12 spacer"></div>


</div>
</div>


@if($owner)
<div id="delete-{{$post->slug}}" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<h4>Confirm Delete for Post {{$post->title}}</h4>
				<a href="/post/{{$post->slug}}/delete" class="btn btn-danger"><i class="fa fa-trash"></i> Confirm Delete</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
@endif



<!-- modal -->
<div id="fsModal"
     class="modal animated bounceIn"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">

  <!-- dialog -->
  <div class="modal-dialog">

    <!-- content -->
    <div class="modal-content">

      <!-- header -->
      <div class="modal-header">
        <h1 id="myModalLabel"
            class="modal-title text-center">
          Select Your Interest
        </h1>
      </div>
      <!-- header -->
      
      <!-- body -->
      <div class="modal-body">
<div class="row">
        
        


@foreach($cats as $cat)


<div class="col-xs-12 col-md-4 col-lg-4 text-center customclass">
            <span style="width: 50px;
    height: 50px;    text-align: center;
    -webkit-border-radius: 70px;
    -moz-border-radius: 70px;
    border-radius: 70px;">           <i class="fas fa-angle-right" style="color:#fe8b6c;"></i>
</span> 
           <a href="/category/{{$cat->slug}}" style="">
{{$cat->title}}
</a>
@if(Auth::check())
@if(Auth::user()->admin)
<div class="btn-group" style="    position: absolute;
    right: 5px;">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-caret-square-down"></i>


  </button>
  <div class="dropdown-menu">
  <a href="/category/{{$cat->slug}}/edit">
<i class="fas fa-pencil-alt"></i>
</a>
	<a href="/category/{{$cat->slug}}/delete">
<i class="fas fa-trash-alt"></i>

</a>
  </div>
</div>					
								@endif
								@endif
        </div>

								
							@endforeach








       
    </div>
      
      </div>
      <!-- body -->

      <!-- footer -->
      <div class="modal-footer hidden">
        <button class="btn btn-secondary"
                data-dismiss="modal">
          close
        </button>
        <button class="btn btn-default">
          Default
        </button>
        <button class="btn btn-primary">
          Primary
        </button>
      </div>
      <!-- footer -->

    </div>
    <!-- content -->

  </div>
  <!-- dialog -->

</div>
<!-- modal -->
@stop
