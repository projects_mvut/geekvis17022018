@extends('layouts.pageWide')
@section('pageTitle', 'BEST FREE Platform for GATE, IBPS, SSC CGL, GRE, GMAT, CAT, IIT JEE, IMU CET preparation.')
<!-- dd animated slideInRight -->
@section('pagecontent')

<!-- <script>
navAni();
</script>
 --><section class="color-orange">


			<div class="caption vertical-align text-center">
<img src="/images/FB54.png" class="img-responsive" style="margin:0 auto; width:80px">
<h3 style="color:#ff8b6c;font-family: 'Encode Sans Condensed', sans-serif;text-transform:uppercase;font-weight:900">A platform where we love and encourage inquisitiveness.</h3>
<h4 style="color:#fff;font-family: 'Encode Sans Condensed', sans-serif;">Join the learning revolution and connect with best mentors and authors.</h4>
<a href="/register" class="btn btn-default btn-main">Sign up for FREE</a>
			</div>



			<div class="outer-wrapper hidden">
				<div class="heading-large text-center">
					<h1>Welcome to GeekVis</h1>
					<br>
				</div>
				<div class="col-md-4 col-sm-4 white-border">
					<div class="inner-wrapper text-center">
						<div class="image-wrapper-block">
							<img src="/images/icon-3.png">
						</div>
						<h2>Participate in question solving</h2>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 white-border">
					<div class="inner-wrapper text-center">
						<div class="image-wrapper-block">

							<img src="/images/icon-1.png" width="185">
						</div>
						<h2>Improve your exam scores</h2>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 white-border">
					<div class="inner-wrapper text-center">
						<div class="image-wrapper-block">

							<img src="/images/icon-2.png" width="140">
						</div>
						<h2>Interact with the best mentors</h2>
					</div>
				</div>
			</div>
	
</section>
<section class="light-section">
	<div class="container">
		<div class="row">
			<div class="outer-wrapper">
				<div class="col-md-12">
					<div class="heading-bar">
						<h3>Popular Categories</h3>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
					<a href="/category/cryptocurrency">
						<div class="catg-wrapper bottom-shadow">
							<img src="/images/icons/bitcoin.png" width="80">
							<p>Cryptocurrency</p>
						</div>
					</a>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
					<a href="/category/gate-preparation">
						<div class="catg-wrapper bottom-shadow">
							<img src="/images/icons/GATE-Preparation.png" width="80">
							<p>Gate Preparation</p>
						</div>
					</a>
				</div>
				<div class="spacer visible-xs"></div>

				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
					<a href="/category/ielts-pte">
						<div class="catg-wrapper bottom-shadow">
							<img src="/images/icons/online-learning.png" width="80">
							<p>IELTS</p>
						</div>
					</a>
				</div>

				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
					<a href="/category/heat-transfer">
						<div class="catg-wrapper bottom-shadow">
							<img src="/images/icons/heat-transfer.png" width="80">
							<p>Heat Transfer</p>
						</div>
					</a>
				</div>
				<div class="spacer visible-sm"></div>

				<div class="clearfix visible-xs"></div>
				<div class="spacer visible-xs"></div>
				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/psychology">
						<div class="catg-wrapper bottom-shadow">
							<img src="/images/icons/idea.png" width="80">
							<p>Psychology</p>
						</div>
					</a>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
					<a href="/category/industrial-engineering">
						<div class="catg-wrapper bottom-shadow">
							<img src="/images/icons/industrial-engg.png" width="80">
							<p>Industrial Engineering</p>
						</div>
					</a>
				</div>

				<div class="spacer hidden-sm"></div>


				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/theory-of-machines">
					<div class="catg-wrapper bottom-shadow">
						<img src="/images/icons/machine-design.png" width="80">
						<p>Machine Design</p>
					</div>
					</a>
				</div>

				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/manufacturing-engineering">
					<div class="catg-wrapper bottom-shadow">
						<img src="/images/icons/manufacturing.png" width="80">
						<p>Manufacturing</p>
					</div>
					</a>
				</div>

				<div class="spacer visible-xs visible-sm"></div>

				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/mechanical-engineering">

					<div class="catg-wrapper bottom-shadow">
						<img src="/images/icons/mechanical-engineering.png" width="80">
						<p>Mechanical Engineering</p>
					</div>
					</a>
				</div>

				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/gre-math">

					<div class="catg-wrapper bottom-shadow">
						<img src="/images/icons/library.png" width="80">
						<p>GRE</p>
					</div>
					</a>
				</div>

				<div class="spacer visible-xs"></div>

				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/theory-of-machines">

					<div class="catg-wrapper bottom-shadow">
						<img src="/images/icons/theory-of-machines.png" width="80">
						<p>Theory of Machines</p>
					</div>
					</a>
				</div>
				<div class="col-md-2 col-sm-3 col-xs-6 categ-animate text-center">
				<a href="/category/thermodynamics">

					<div class="catg-wrapper bottom-shadow">
						<img src="/images/icons/Thermodynamics.png" width="80">
						<p>Thermodynamics</p>
					</div>
					</a>
				</div>
				<div class="text-center" style="margin-top:20px;float:left;width:100%">
					<a href="#" data-toggle="modal" data-target="#fsModal" class="btn gv-btn">View More</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="testimonial-section hidden-xs">
	<div class="container">
		<div class="row">
			<div class="outer-wrapper text-center">
				<div class="col-md-12">
					<div class="heading-bar" style="color:#4e4e4e">
						<h3>What Geeks Say</h3>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 mb-r testi-animate">
					<div class="testimonial">
						<div class="avatar">
							<img src="/images/testi/sushant.jpg" class="img-circle img-fluid">
						</div>
						<h4>Sushant Vijan</h4>
						<p><i class="fa fa-quote-left"></i>The website provides you with a great way to connect in realtime with fellow students who are preparing for GATE from all across India. The website has helped me to practice on a daily basis and the notes provided are awesome.</p>
					</div>
				</div>

				<div class="col-md-3 col-sm-3 mb-r testi-animate">
					<div class="testimonial">
						<div class="avatar">
							<img src="/images/testi/sourav.jpg" class="img-circle img-fluid">
						</div>
						<h4>Sourav Upadhyay</h4>
						<p><i class="fa fa-quote-left"></i> If you want to save money and time for GATE preparation, GeekVis is the place to be. Helpful articles on GATE preparation and regular practice questions help a lot. </p>
					</div>
				</div>

				<div class="col-md-3 col-sm-3 mb-r testi-animate">
					<div class="testimonial">
						<div class="avatar">
							<img src="/images/testi/rohit.jpg" class="img-circle img-fluid">
						</div>
						<h4>Rohit Singh</h4>
						<p><i class="fa fa-quote-left"></i>This platform helped me a lot during my GATE 2017 preparation. The questions shared are very good and brings in a regularity to your prep. Also, it saved me a lot of money as all things on GeekVis are free. Thanks a lot!</p>
					</div>
				</div>			       
				<div class="col-md-3 col-sm-3 mb-r testi-animate">
					<div class="testimonial">
						<div class="avatar">
							<img src="/images/testi/param.jpg" class="img-circle img-fluid">
						</div>
						<h4>Parampreet Singh</h4>
						<p><i class="fa fa-quote-left"></i>GeekVis is the only platform in India, which has everything FREE and yet the content is meaningful and helpful for GATE preparation. I also prepared for IBPS and SSC exams and the resources provided has been great.</p>
					</div>
				</div>			      
















 
			</div>
		</div>
	</div>
</section>



<section class="testimonial-section visible-xs">
	<div class="container">
		<div class="row">
			<div class="outer-wrapper text-center">
				<div class="col-md-12">
					<div class="heading-bar" style="color: #4e4e4e">
						<h3>Our Testimonials</h3>
					</div>
				</div>
				<div class="col-md-8 col-md-offset-2">
					<div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
					<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
						<!-- Carousel indicators -->
						<ol class="carousel-indicators hidden-xs">
							<li data-target="#fade-quote-carousel" data-slide-to="0"></li>
							<li data-target="#fade-quote-carousel" data-slide-to="1"></li>
							<li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
							<li data-target="#fade-quote-carousel" data-slide-to="3"></li>
						</ol>
						<!-- Carousel items -->
						<div class="carousel-inner">
							<div class="item">
								<div class="col-md-3 col-sm-3 mb-r">
									<div class="testimonial">
										<div class="avatar">
											<img src="/images/testi/sushant.jpg" class="img-circle img-fluid">
										</div>
										<h4>Sushant Vijan</h4>
										<p><i class="fa fa-quote-left hidden-xs"></i>The website provides you with a great way to connect in realtime with fellow students who are preparing for GATE from all across India. The website has helped me to practice on a daily basis and the notes provided are awesome.</p>
									</div>
								</div>

							</div>
							<div class="item">
								<div class="col-md-3 col-sm-3 mb-r">
									<div class="testimonial">
										<div class="avatar">
											<img src="/images/testi/sourav.jpg" class="img-circle img-fluid">
										</div>
										<h4>Sourav Upadhyay</h4>
										<p><i class="fa fa-quote-left hidden-xs"></i> If you want to save money and time for GATE preparation, GeekVis is the place to be. Helpful articles on GATE preparation and regular practice questions help a lot. </p>
									</div>
								</div>
							</div>
							<div class="active item">
								<div class="col-md-3 col-sm-3 mb-r">
									<div class="testimonial">
										<div class="avatar">
											<img src="/images/testi/rohit.jpg" class="img-circle img-fluid">
										</div>
										<h4>Rohit Singh</h4>
										<p><i class="fa fa-quote-left hidden-xs"></i>This platform helped me a lot during my GATE 2017 preparation. The questions shared are very good and brings in a regularity to your prep. Also, it saved me a lot of money as all things on GeekVis are free. Thanks a lot!</p>
									</div>
								</div>			       
							</div>
							<div class="item">
								<div class="col-md-3 col-sm-3 mb-r">
									<div class="testimonial">
										<div class="avatar">
											<img src="/images/testi/param.jpg" class="img-circle img-fluid">
										</div>
										<h4>Parampreet Singh</h4>
										<p><i class="fa fa-quote-left hidden-xs"></i>GeekVis is the only platform in India, which has everything FREE and yet the content is meaningful and helpful for GATE preparation. I also prepared for IBPS and SSC exams and the resources provided has been great.</p>
									</div>
								</div>		










	 
							</div>

						</div>
					</div>
				</div>							
			</div>
		</div>
	</div>
</section>

<section class="testimonial-section" style="padding:20px 0;">
	
	<div class="container">

<div class="row">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- geekvis code -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-4564993389423981"
     data-ad-slot="5295761858"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> 

</div>
	</div>
</section>

  



<!-- view modal -->

<!-- modal -->
<div id="fsModal"
     class="modal animated bounceIn"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">

  <!-- dialog -->
  <div class="modal-dialog">

    <!-- content -->
    <div class="modal-content">

      <!-- header -->
      <div class="modal-header">
        <h1 id="myModalLabel"
            class="modal-title text-center">
          Select Your Interest
        </h1>
      </div>
      <!-- header -->
      
      <!-- body -->
      <div class="modal-body">
<div class="row">
        
        


@foreach($cats as $cat)


<div class="col-xs-12 col-md-4 col-lg-4 text-center customclass">
            <span style="width: 50px;
    height: 50px;    text-align: center;
    -webkit-border-radius: 70px;
    -moz-border-radius: 70px;
    border-radius: 70px;">           <i class="fas fa-angle-right" style="color:#fe8b6c;"></i>
</span> 
           <a href="/category/{{$cat->slug}}" style="">
{{$cat->title}}
</a>
@if(Auth::check())
@if(Auth::user()->admin)
<a href="/category/{{$cat->slug}}/edit">
<i class="fa fa-pencil"></i>
</a>
	<a href="/category/{{$cat->slug}}/delete">
<i class="fa fa-trash"></i>
</a>						
								@endif
								@endif
        </div>

								
							@endforeach








       
    </div>
      
      </div>
      <!-- body -->

      <!-- footer -->
      <div class="modal-footer hidden">
        <button class="btn btn-secondary"
                data-dismiss="modal">
          close
        </button>
        <button class="btn btn-default">
          Default
        </button>
        <button class="btn btn-primary">
          Primary
        </button>
      </div>
      <!-- footer -->

    </div>
    <!-- content -->

  </div>
  <!-- dialog -->

</div>


<script type="text/javascript">
	var page = window.location.pathname;

if(page == '/'){
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('nav').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('nav').removeClass('nav-down').addClass('nav-up');
        	if(st > 80)
        	{

        $('nav').addClass('navbar-no-height');
                    $('nav .navbar-brand').addClass('navbar-brand-small');

    }

    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
        	  if(st < 80)
        	{
            $('nav').removeClass('nav-up').addClass('nav-down');

          
            $('nav').removeClass('navbar-no-height');
            $('nav .navbar-brand').removeClass('navbar-brand-small');

    }

        }
    }
    
    lastScrollTop = st;
}

}


else{
$("nav").css({"height": "50px", "margin-top": "0", "background": "#fff","box-shadow":"0 0 10px rgba(0,0,0,.1)"});
$("nav .container").css({"box-shadow": "0px 0px 0px #000"});
$("body").css({"margin-top": "40px"});

}
</script>
@stop
