@extends('layouts.page')

@section('pagecontent')

{!!Form::open(['url'=>'uploaddata','files'=>'true'])!!}
<div class="form-group">
	{!! Form::label('Key') !!}
	{!! Form::text('key',null,['placeholder'=>'Key','class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('Data File') !!}
	{!! Form::file('datafile', null,['value'=>'Upload Data File']) !!}
</div>
<div class="form-group">
	{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
</div>
{!!Form::close()!!}
@stop