@extends('layouts.page')

@section('pagecontent')

<div id="page">
	<div class="ribbon"  style="background-color: #558188;">Contact Us</div>
	<div class="content">
		<p>Visit us at:</p>
		<p>GeekVis,</p>
		<p>F-452, Industrial Area, Phase 8 B,</p>
		<p>Mohali - 160055, Punjab, India.</p>
		<p>Contact numbers:&nbsp;</p>
		<p>+91-8283865488</p>
		<p>+91-9779434433</p>
		<p>Email is at <a href="mailto:igeekvis@gmail.com" target="_blank">igeekvis@gmail.com</a>.&nbsp;</p>
	</div>
</div>

@stop