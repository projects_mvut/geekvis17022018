@extends('layouts.page')

@section('pagecontent')

<div id="page">
	<div class="ribbon" style="background-color: #558188;">About Us</div>
	<div class="content">
		<div>Welcome to GeekVis, a platform where we love and encourage INQUISITIVENESS. Join us in our mission to foster strong learner-educator-instructor community by creating a platform to share your queries relating to anything and everything.</div>
		<div><span style="color: #333333; font-family: roboto, uilanguagefont, arial, sans-serif;"><br /></span>
			<p>Whether you are a student preparing for GATE or IBPS PO or GRE or GMAT, or a teacher/ instructor who loves to interact with the students for knowledge sharing, you will love it here at GeekVis.</p>
			<p>- Get connected to an active community of students preparing for various competitive exams like GATE, IBPS PO, SSC CGL.<br />- Get connected to learning enthusiasts from all over the world.&nbsp;<br />- Connect to people as per your category of interest.</p>
		</div>
	</div>
</div>

@stop