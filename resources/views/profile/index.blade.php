@extends('layouts.pageFluid')

@section('pagecontent')

<div id="profile">
	<div class="">
<div class="col-md-12">
		<div class="col col-sm-3">
			<div class="side1">
				<img src="{{$user->imgUrl}}" class="proimg">
				<h3>{{$user->name}}</h3>
				<p>{{$user->bio}}</p>
				@if(Auth::check())
				@if(Auth::user()->id==$user->id)
				<a href="/dashboard/{{$user->username}}/profile" class="btn btn-info">Edit</a>
				@else
				@if($follow)
				<a href="/unfollow/{{$user->username}}" class="btn btn-warning">Unfollow</a>
				@else
				<a href="/follow/{{$user->username}}" class="btn btn-info">Follow</a>
				@endif
				@endif
				@else
				<a href="/login" class="btn btn-info">Login To Subscribe</a>
				@endif
				<hr>
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#followersModal">
					<i class="fa fa-users"></i> {{$followers->count()}} Subscribers
				</button>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#followingModal">
					<i class="fa fa-user"></i> {{$following->count()}} Subscribed
				</button>

				<div id="followersModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Subscribers</h4>
							</div>
							<div class="modal-body">
								<div class="followers">
									@if(!$followers->isEmpty())
									@foreach($followers as $userbox)
									@include('partials.elements.userbox')
									@endforeach
									@else
									<p>No Subscribers</p>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="followingModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Subscribed</h4>
							</div>
							<div class="modal-body">
								<div class="following">
									@if(!$following->isEmpty())
									@foreach($following as $userbox)
									@include('partials.elements.userbox')
									@endforeach
									@else
									<p>None Subscribed</p>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>



			</div>
		</div>
		<div class="col col-sm-9">
		<div class="row equal">

			

<!-- if posts empty -->
			@if($posts->isEmpty())
			<div id="empty">
				<h2>No activity yet!</h2>
				<h4>Got a doubt on your mind?</h4>
				<img src="/images/stud.png" alt="">
				@if(Auth::user())
				@else
				<a href="/login" class="btn btn-success">Start Here</a>
				@endif
				<h4>Connect with co-learners on topics of interest.</h4>
				<img src="/images/brain.png" alt="">
				<h4>Recommended users to follow:</h4>
				<div class="row">
					@foreach($editors as $editor)
					<div class="col col-sm-4 rco"><a href="/profile/{{$editor->username}}">
						<img src="{{$editor->imgUrl}}" alt="">
						<h5>{{$editor->name}}</h5>
					</a>
				</div>
				@endforeach
			</div>
		</div>


<!-- else part -->
		@else
<?php $i=0; ?>
		@foreach($posts as $post)
			@if($i%3==0)
			<div id="post" class="postbox bottom-margin col-md-4 col-sm-4 col-lg-4" style="padding: 0px 0; padding-bottom: 0">
			@else
			<div id="post" class="postbox bottom-margin col-md-8 col-sm-8 col-lg-8" style="padding: 0px 0; padding-bottom: 0">
			@endif
		@include('partials.elements.postbox')
			<?php $i++; ?>
		@endforeach
		@endif







</div>
	<div style="width:100%" class="text-center"> 	
								{!! $posts->render() !!}
								</div>



<hr>


<!-- 		if draft -->
			@if($drafted)
			<div class="" id="drafted">
				<?php $j=0; ?>
				@foreach($drafted as $post)
				@if($j%3==0)
			<div id="post" class="postbox bottom-margin col-md-4 col-sm-4 col-lg-4" style="padding: 0px 0; padding-bottom: 0">
			@else
			<div id="post" class="postbox bottom-margin col-md-8 col-sm-8 col-lg-8" style="padding: 0px 0; padding-bottom: 0">
			@endif
				@include('partials.elements.postbox')
							<?php $j++; ?>
				@endforeach
			</div>
			@endif






		</div>
	</div>
</div>
</div>
</div>
@stop