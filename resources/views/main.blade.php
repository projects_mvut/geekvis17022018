<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta property="fb:pages" content="1042666339184878" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  @if(isset($keywords) && $keywords)
  <meta name="keywords" content="{{$keywords}}" />
  @else
  <meta name="keywords" content="free gate preparation, gate mechanical free notes, gate preparation tips, free ibps po preparation, ibps po practice questions, gate practice questions, ssc cgl free preparation, ssc cgl mock tests, gate mock tests, gate, gate mechanical engineering" />
  @endif
  @if(isset($description) && $description)
  <meta name="description" content="{{$description}}"/>
  @else
  <meta name="description" content="Connect with BEST mentors and students to give a boost to your competitive exam preparation. "/>
  @endif
  <link href="/images/favicon.png" rel="icon" type="image/x-icon" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script defer src="https://use.fontawesome.com/releases/v5.0.1/js/all.js"></script>
  <script type="text/javascript">

</script>


<script type="text/javascript">
//paste this code under the head tag or in a separate js file.
  // Wait for window load
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
  });
  </script>


  @if(isset($title) && $title)
  <title>{{$title}}</title>
  @else
  <title>@yield('pageTitle')</title>
  @endif
  <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
  @yield('headinclude')
<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/animatecss/2.1.0/animate.min.css"> -->
@if(Auth::check() && Request::path() == '/')

    <!-- <script type="text/javascript">
    $(window).on('load',function(){
        $('#fsModal').modal('show');
    });
</script> -->
@endif
</head>
<body data-target=".nav-stacked" data-offset="1150">
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="fas fa-chevron-up"></span></a>

  <div id="fold">
<div class="se-pre-con"></div>

    @include('partials.elements.nav')
     <div id="page-content">

    @yield('maincontent')
    @include('partials.notifications')
  </div>
  @include('partials.elements.footer')
  @if(Auth::check())
  <div id="userId" data-uri="{{env('SITE_URL')}}/profile/{{Auth::user()->username}}"></div>
  @endif
  <script src="{{ elixir('js/all.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.3.9/jquery.jscroll.js"></script>
<script type="text/javascript">
    $('.com ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script>
  @yield('footinclude')
  @if(Auth::check())
  <script src="//cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.min.js"></script>
  <script>
  var socket = io("{{env('SOCKET_URL')}}");
  socket.on("star-channel-{{Auth::user()->username}}", function(message){
    console.log('starred',message);
    $('#star').show();
  });
  </script>
  @endif
  <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-83224716-1', 'auto');ga('send', 'pageview');</script>
<script type="text/javascript">
  var chitikaPublisherName = "mindvis";

  var $chitikaAdHolders = document.getElementsByClassName('chitika-ads-container');
  var chitikaAdholdersCount = $chitikaAdHolders.length;

  var setAdUnitDimensions = function (unit, width, height) {
    unit.width = width;
    unit.height = height;
  };

  var insertChitikaAd = function ($holder) {
    var adWidth = 0;
    if ($holder.getBoundingClientRect().width) {
      adWidth = $holder.getBoundingClientRect().width; // for modern browsers
    } else {
      adWidth = $holder.offsetWidth; // for old IE
    }

    var unit = {
      "calltype":"async[2]",
      "publisher": chitikaPublisherName,
      "sid": "Chitika Default"
    };
    if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; };
    if ( adWidth >= 728 )
      setAdUnitDimensions(unit, 728, 90);
    else if ( adWidth >= 468 )
      setAdUnitDimensions(unit, 600, 160);
    else if ( adWidth >= 382 )
      setAdUnitDimensions(unit, 300, 250);

    else if ( adWidth >= 336 )
      setAdUnitDimensions(unit, 300, 250);
    else if ( adWidth >= 300 )
      setAdUnitDimensions(unit, 16, 600);
    else if ( adWidth >= 250 )
      setAdUnitDimensions(unit, 160, 600);
    else if ( adWidth >= 200 )
      setAdUnitDimensions(unit, 160, 600);
    else if ( adWidth >= 180 )
      setAdUnitDimensions(unit, 160, 600);
else if ( adWidth >= 100 )
      setAdUnitDimensions(unit, 160, 600);
    else
      setAdUnitDimensions(unit, 125, 125);
    var placement_id = window.CHITIKA.units.length;
    window.CHITIKA.units.push(unit);
    $holder.innerHTML = '<div id="chitikaAdBlock-' + placement_id + '"></div>';
  };

  for(var i=0; i<chitikaAdholdersCount; i++){
    insertChitikaAd($chitikaAdHolders[i]);
  }
</script>



<script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
<script type="text/javascript">
$(window).scroll(function () {
    $('#secondary .nav-stacked.affix').width($('#secondary').width());
});

</script>

<script>
if ( window.location.pathname == '/' ){
$('#wall').hide();
} 
else {
  $('.customclass2').hide();

  $('.heading-categ').hide();

}
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>


<script type="text/javascript">
  
  $(document).ready(function () {


    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));
  
    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));  

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '80%';
    var menuneg = '-100%';
    var slideneg = '-80%';


    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });


        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');


        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');


    });


    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


    $(window).on("resize", function () {

        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }


    });




});


</script>
<script type="text/javascript">
  $(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});
</script>

</div>

</body>
</html>
