<script src="/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
	selector:'textarea',
	theme: "modern",
	skin: 'light',
	external_plugins: {'graphTinymcePlugin': '/tinymce/plugins/graph-tinymce-plugin/plugin.min.js'}, // Add plugin to Tinymce

//Allow html script, style, etc tags to work in the editors
extended_valid_elements: 'pre[*],script[*],style[*]', 
valid_children: "+body[style|script],pre[script|div|p|br|span|img|style|h1|h2|h3|h4|h5],*[*]",
valid_elements : '*[*]',


	plugins:["advlist autolink lists link image charmap print preview hr anchor pagebreak","searchreplace wordcount visualblocks visualchars code fullscreen","insertdatetime media nonbreaking save table contextmenu directionality","emoticons template paste textcolor colorpicker textpattern imagetools equa mention"],
	toolbar1:"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image equa | graphTinymcePlugin",
	toolbar2:"print preview media | forecolor backcolor emoticons",
	graph_uploader: function (file, cb) {
      // Here is your uploader logic, start to upload you image here like that:
 
      // yourUploader.sendIMG(file.blob)
      //   .then(function(url){
      //      // Take a look at "class='tinymce-graph'" and "graph-data='" + file.graphData + "'", it is really important to keep it in the tag - that's way you able to edit your graph.
      //      cb("<img class='tinymce-graph' graph-data='" + file.graphData + "' width='" + file.width + "' height='" + file.height + "' src='" + url + "' />");
      //   });
 
      // or just put SVG-html into your content. Example:
      cb(file.html);
    },
	image_advtab:!0,
	mentions: {
		source: function (query, process, delimiter) {
			$.getJSON('/json/users', function (usersData) {
				process(usersData)
			});
		},
		delay:10
	}
});
</script>
