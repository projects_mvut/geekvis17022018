	<div class="form-group">
		{!! Form::label('name', 'Title', array('class' => 'col-xs-2 control-label')) !!}
		            <div class="col-xs-10">

		{!! Form::text('title',null,['placeholder'=>'Title','class'=>'form-control']) !!}
		            </div>

	</div>
	<div class="form-group">
		{!! Form::label('name', 'Category', array('class' => 'col-xs-2 control-label')) !!}
				            <div class="col-xs-10">
		{!! Form::select('category_id',$select_category,null , ['class'=>'form-control']) !!}
				            </div>

	</div>
	@if(Auth::user()->admin)
	<div class="form-group">
		{!! Form::label('name', 'Slug', array('class' => 'col-xs-2 control-label')) !!}
		<div class="col-xs-10">

		{!! Form::text('slug',null,['placeholder'=>'Slug','class'=>'form-control']) !!}
		</div>
	</div>
	@endif
	<div class="form-group">
		{!! Form::label('name', 'Type', array('class' => 'col-xs-2 control-label')) !!}
		<div class="col-xs-10">
		{!! Form::radio('type', 'question',true) !!}
		Question
		{!! Form::radio('type', 'article') !!}
		Article
		{!! Form::radio('type', 'quiz') !!}
		Quiz
		</div>
	</div>

		<div class="form-group">
			{!! Form::label('name', 'Image (Optional)', array('class' => 'col-xs-2 control-label')) !!}
			<div class="col-xs-10">
			{!! Form::file('imgUrl', null,['value'=>'Upload Image']) !!}
			</div>
		</div>
		@if(Auth::user()->admin)
		<div class="form-group">
			{!! Form::label('name', 'Boost Image', array('class' => 'col-xs-2 control-label')) !!}
			<div class="col-xs-10">
			{!! Form::file('boostUrl', null,['value'=>'Upload Image']) !!}
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('name', 'CSV (Optional)', array('class' => 'col-xs-2 control-label')) !!}
			<div class="col-xs-10">
			{!! Form::file('csvUrl', null,['value'=>'Upload CSV']) !!}
			</div>
		</div>
		
		@endif

<div class="form-group">
<div class="col-md-	10 col-md-offset-2">
	{!! Form::textarea('content',null,['placeholder'=>'Enter the content','class'=>'form-control']) !!}
	</div>
</div>
@if(Auth::user()->admin)
<div class="form-group">
	{!! Form::label('name', 'Keywords', array('class' => 'col-xs-2 control-label')) !!}
	<div class="col-md-10">
	{!! Form::text('keywords',null,['placeholder'=>'SEO Keywords','class'=>'form-control']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('name', 'Description', array('class' => 'col-xs-2 control-label')) !!}
	<div class="col-md-10">
	{!! Form::text('description',null,['placeholder'=>'SEO Description','class'=>'form-control']) !!}
	</div>
</div>
@endif

<div class="form-group">
	<div class="col-md-10 col-md-offset-2"> 
		{!! Form::submit('Submit',['class'=>'btn btn-primary','name'=>"submit"]) !!}
		@if(Auth::user()->admin)
		{!! Form::submit('Draft',['class'=>'btn btn-info','name'=>"submit"]) !!}
		@endif
	</div>
</div>

{!!Form::close()!!}
