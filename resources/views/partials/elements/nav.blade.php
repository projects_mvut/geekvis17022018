 <div class="navbar visible-xs	" role="navigation" id="slide-nav">
  <div class="container">
   <div class="navbar-header">
    <a class="navbar-toggle"> 
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
     </a>
    <a class="navbar-brand" href="/">Geekvis</a>
   </div>
   <div id="slidemenu">
     		@include('partials.elements.search')

        <!--   <form class="navbar-form navbar-right" role="form">
            <div class="form-group">
              <input type="search" placeholder="search" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
          </form> -->
     
    <ul class="nav navbar-nav hidden">
     <li class="active"><a href="#">Home</a></li>
     <li><a href="#about">About</a></li>
     <li><a href="#contact">Contact</a></li>
     <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
      <ul class="dropdown-menu">
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li class="divider"></li>
       <li class="dropdown-header">Nav header</li>
       <li><a href="#">Separated link</a></li>
       <li><a href="#">One more separated link</a></li>
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li class="divider"></li>
       <li class="dropdown-header">Nav header</li>
       <li><a href="#">Separated link</a></li>
       <li><a href="#">One more separated link</a></li>
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li class="divider"></li>
       <li class="dropdown-header">Nav header</li>
       <li><a href="#">Separated link test long title goes here</a></li>
       <li><a href="#">One more separated link</a></li>
      </ul>
     </li>
    </ul>
    		@if(Auth::check())
			<ul class="nav navbar-nav navbar-right">
				<li class="tanbell hidden-xs"><a href="#"><i class="fa fa-bell-o"></i></a>
					@if(Auth::user()->star)
					<span id="star"><i class="fa fa-star"></i></span>
					@else
					<span id="star" class="ecli"><i class="fa fa-star"></i></span>
					@endif
				</li>
				<span id="bell-temp" class="hidden-xs">
					<span class="container inn">
						<a class="rang" href="/notifications">See All</a>
						<span class="clbell"><i class="fa fa-times"></i></span>
						@foreach($noti as $n)
						<span class="ring">
							<a href="/profile/{{$n->user->username}}">{{$n->user->name}}</a>
							@if($n->type)
							Commented On
							@else
							Liked
							@endif
							<a href="/post/{{$n->post->slug}}">{{$n->post->title}}</a>
						</span>
						@endforeach
					</span>
				</span>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{Auth::user()->imgUrl}}" alt="" class="img-circle
					"  width="25"> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/profile/{{Auth::user()->username}}">{{Auth::user()->name}}</a></li>
						<li><a href="/logout">Sign out</a></li>
					</ul>
				</li>
			</ul>


			
			
			


			@else
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/login">Sign in</a></li>
				<li><a href="/register">Sign up</a></li>
				
			</ul>
			@endif
   </div>
  </div>
 </div>
  

<nav class="navbar navbar-fixed-top custom-nav nav-down hidden-xs">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="z-index:1;">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			</button>
			<a class="navbar-brand hidden-xs" href="/">Geekvis</a>
			<a class="navbar-brand visible-xs" href="/"><img src="/images/favicon.png" width="40" style="margin-top: -10px;"></a>

		</div>



		
		<div class="collapse navbar-collapse" id="myNavbar">
		<div class="hidden-xs">
		@include('partials.elements.search')
		</div>
			@if(Auth::check())
			<ul class="nav navbar-nav navbar-right">
				<li class="tanbell"><a href="#"><i class="far fa-bell fa-2x"></i>

</a>
					@if(Auth::user()->star)
					<span id="star"><i class="fa fa-star"></i></span>
					@else
					<span id="star" class="ecli"><i class="fa fa-star"></i></span>
					@endif
				</li>
				<span id="bell">
					<span class="container inn">
						<a class="rang" href="/notifications">See All</a>
						<span class="clbell"><i class="fa fa-times"></i></span>
						@foreach($noti as $n)
						<span class="ring">
							<a href="/profile/{{$n->user->username}}">{{$n->user->name}}</a>
							@if($n->type)
							Commented On
							@else
							Liked
							@endif
							<a href="/post/{{$n->post->slug}}">{{$n->post->title}}</a>
						</span>
						@endforeach
					</span>
				</span>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{Auth::user()->imgUrl}}" alt="" class="img-circle
					"  width="25"> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/profile/{{Auth::user()->username}}">{{Auth::user()->name}}</a></li>
						<li><a href="/logout">Sign out</a></li>
					</ul>
				</li>
			</ul>


			
			
			


			@else
			<ul class="nav navbar-nav navbar-right hidden-xs">
				<li><a href="/login">Sign in</a></li>
				<li><a href="/register">Sign up</a></li>
				
			</ul>
			@endif
		</div>
	</div>
</nav>



