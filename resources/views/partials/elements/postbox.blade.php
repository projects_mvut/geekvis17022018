<!-- start of postbox -->
	<!-- start of first block -->


	<a href="/post/{{$post->slug}}" style="display:block;position:absolute;bottom:15px;z-index: 1; right:15px; background:#fe8b6c; color:#fff; padding:10px 15px;" class="title hidden-xs">
					<span class="info-blk pull-right color-bg"> 
						READ MORE
					</span>
					</a>






		
		<!-- start of second block -->
		<div class="gv-post-image">
			@if($post->imgUrl)
			@if($post->type=='question')
			<img src="{{$post->imgUrl}}" class="postimg postModalImage" width="100%" style="height:250px;object-position: top;" data-post="{{$post->slug}}">
			<div class="modalQuestion modalQuestionHolder-{{$post->slug}}">
				<img src="{{$post->imgUrl}}" alt="" class="img-responsive">
			</div>
			@else
			<a href="/post/{{$post->slug}}">
				<img src="{{$post->imgUrl}}" class="postimg img-responsive" width="100%">
			</a>
			@endif
			@endif
		</div>



	<div class="col-md-12 col-sm-12 col-xs-12">
	<!--start of post-title -->
		<div class="gv-post-title">
			<div class="cl hidden" style="min-width: 50px;max-width: 50px; padding-left: 0">
				<a href="/profile/{{$post->user->username}}">
					<img src="{{$post->user->imgUrl}}" alt="" class="img-circle
					"  width="100%">
				</a>
			</div>
			<div class="cl">
				<a href="/post/{{$post->slug}}" class="title"><h3>{{$post->title}}</h3></a>
			</div>
		</div> 
		<!--end of post-title -->
		<!--start of post-info -->
		<div class="gv-post-info hidden">
			<div class="col-sm-12">
				<div class="row">
					<!-- start of info blk username -->
					<span class="info-blk"> 
						{{$post->user->name}}
					</span>
					<!-- end of info blk social -->
					<!-- start of info blk category name -->
					<span class="info-blk"> 
						@if($post->category)
						@if($post->category->slug)
						<a href="/category/{{$post->category->slug}}" class="cat">
							@else
							<a href="/category/{{$post->category->id}}" class="cat">
								@endif
								<span class="cat">
									{{$post->category->title}}
								</span>
							</a>
							@else
							<a href="#" class="cat">
								<span class="cat">
								</span>
							</a>
							@endif
						</span>
						<!-- end of info blk category name -->
						<!-- start of info blk date -->
						<span class="info-blk"> 
							Posted: <span>{{$post->created_at->diffForHumans()}}</span>
						</span>
						<!-- end of info blk date -->
						<!-- start of info blk social -->
						<span class="info-blk" style="padding:0"> 
							<div class="dropdown">
								<a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="display: block;padding:10px;">
									<img src="/images/share.svg" width="16">
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu social" aria-labelledby="dropdownMenu1">
									<li> <a href="javascript:window.print()"><img src="/images/printer.svg" width="17"> Print</a>  </li>
									<li> <a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->twitter()}}" target="_blanks" class="twitter"><img src="/images/twitter.svg" width="17"> Share on Twitter</a></li>
									<li><a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->facebook()}}" target="_blank" class="facebook"><img src="/images/facebook.svg" width="17"> Share on Facebook</a></li>
									<li><a href="{{Share::load(env('SITE_URL').'/post/'.$post->slug, $post->title)->gplus()}}" target="_blank" class="google"><img src="/images/google-plus.svg" width="17"> Share on Google+</a></li>
								</ul>
							</div>
						</span>
						<!-- end of info blk social -->
					</div>
				</div>
			</div>
			<!--end of post-info -->			
</div>		
		<!-- end of first block -->
		
		
		
		
		
		
		<!-- end of second block -->
		
		
		
		
		<!-- start of third block -->		
		<span class="gv-post-content limit_para">
			<p>
				<?php $string = $post->content ?>
				{!! str_limit($string, $limit = 110, $end = '...') !!}
			</p>
		</span>
		<!-- end of third block -->



	<a href="/post/{{$post->slug}}" class="intract-postbox" style="display:none;">
			<div class="col-sm-4 col-xs-4 text-center"><span class="sat"><i class="fa fa-thumbs-o-up"></i> {{$post->likes->count()}}</span></div>
			<div class="col-sm-4 col-xs-4 text-center"><span class="sat"><i class="fa fa-comments-o"></i> {{$post->comments->count()}} </span></div>
			<div class="col-sm-4 col-xs-4 text-center">
				<span><i class="fa fa-eye"></i> {{$post->views}}</span>
			</div>
		</a>

		
		
		
		
		<!-- start of forth block -->
		<div class="col-sm-12">
			<div class="">
				<span class="gv-post-info">
					<span class="info-blk no-bg"> 
						Likes: {{$post->likes->count()}}
					</span>
					<span class="info-blk no-bg"> 
						Views {{$post->views}} 
					</span>
					<span class="info-blk no-bg"> 
						Comments {{$post->comments->count()}}
					</span>
					<a href="/post/{{$post->slug}}" class="title visible-xs">
					<span class="info-blk pull-right color-bg"> 
						READ MORE
					</span>
					</a>
				</span>
			</div>
		</div>
		<!-- end of forth block -->

</div>
<!-- end of postbox -->


