<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="free gate preparation, gate mechanical free notes, gate preparation tips, free ibps po preparation, ibps po practice questions, gate practice questions, ssc cgl free preparation, ssc cgl mock tests, gate mock tests, gate, gate mechanical engineering" />
	<meta name="description" content="Connect with India's BEST mentors and students to give a boost to your competitive exam preparation. "/>
	<link href="/favicon.png" rel="icon" type="image/x-icon" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>GeekVis | India's BEST FREE Platform for GATE, IBPS, SSC CGL, GRE, GMAT, CAT, IIT JEE, IMU CET preparation.</title>
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body>
	<div id="fold" class="container">
		@yield('maincontent')
	</div>
	<script src="{{ elixir('js/all.js') }}"></script>
</body>
</html>