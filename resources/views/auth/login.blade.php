@extends('layouts.page')
@section('pageTitle', 'Login to GeekVis')
@section('pagecontent')






<section class="authorization">
<div class="col-md-10 col-md-offset-1 col-xs-12">
<div class="row equal">
<div class="col-md-4 hidden-xs left">
<div class="inner">
<img src="/images/favicon.png" class="img-responsive" id="owl">
  <h3 class="text-center">Sign In</h3>
</div>
</div>

<div class="col-md-8 col-xs-12 right">

<div class="inner line_c">




<div class="auth">


<form method="POST" action="/login">
{!! csrf_field() !!}
<div class="form-group">
<input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
</div>
<div class="form-group">
<input type="password" name="password" id="password" class="form-control" placeholder="Password">
</div>

<div class="form-group">
<button type="submit" class="btn btn-primary form-control">Sign in</button>
<div style="padding: 6px 0; text-align: center;float: left;width:100%; font-size: 10px;"><a href="/password/email" class="" >Forget your password?</a>
</div>
</form>

<hr>
<div class="text-center">Or Sign in with</div>
<hr>
<a href="/login/facebook" class="btn btn-social btn-f"><i class="fab fa-facebook-f"></i> Facebook</a>
<a href="/login/google" class="btn btn-social btn-g"> <i class="fab fa-google-plus-g"></i> Google</a>
</div>
</div>

</div>

</div>
</div>
</div>
</section>


@stop
