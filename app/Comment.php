<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['content','user_id','post_id','imgUrl'];

    protected $with = ['user','votes'];

    public function post(){
        return $this->belongsTo('App\Post');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function votes(){
        return $this->hasMany('App\Vote');
    }
}
