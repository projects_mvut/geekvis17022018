<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title','slug','content','imgUrl','user_id','minutes','type','category_id','views','boost','boostUrl','keywords','description','published'];

    protected $with = ['comments','likes','user','category'];

    public function scopePublished($query){
            return $query->where('published',true);
    }


    public function scopeDrafted($query){
            return $query->where('published',false);
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function likes(){
        return $this->hasMany('App\Like');
    }

    public function questions(){
        return $this->hasMany('App\Question');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }
}
