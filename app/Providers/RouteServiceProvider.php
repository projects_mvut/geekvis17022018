<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $router->bind('user',function($user){
            return \App\User::where('username',$user)->firstorfail();
        });

        $router->bind('post',function($post){
            return \App\Post::where('slug',$post)->firstorfail();
        });

        $router->bind('category',function($category){
            $cat= \App\Category::where('slug',$category)->first();
            if($cat){
                return $cat;
            }
            return \App\Category::where('id',$category)->firstorfail();
        });

        $router->bind('comment',function($comment){
            return \App\Comment::where('id',$comment)->firstorfail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
