<?php

namespace App\Providers;
use App\Ad;
use App\User;
use App\Post;
use App\Category;
use App\Notification;
use Auth;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer('posts.index', function ($view) {
			// $ad=Ad::where('id',1)->first();
			$select_category=Category::lists('title', 'id')->toArray();
			$boosted=Post::where('boost',1)->get();
			$view->with('cats',Category::all());
			$view->with('select_category',$select_category);
			$view->with('boosted',$boosted);
			// $view->with('ad',$ad);
		});
		view()->composer('posts.show', function ($view) {
			// $ad=Ad::where('id',1)->first();
			$select_category=Category::lists('title', 'id')->toArray();
			$boosted=Post::where('boost',1)->get();
			$view->with('cats',Category::all());
			$view->with('select_category',$select_category);
			$view->with('boosted',$boosted);
			// $view->with('ad',$ad);
		});
		view()->composer('pages.index', function ($view) {
			// $ad=Ad::where('id',1)->first();
			$select_category=Category::lists('title', 'id')->toArray();
			$boosted=Post::where('boost',1)->get();
			$view->with('cats',Category::all());
			$view->with('select_category',$select_category);
			$view->with('boosted',$boosted);
			// $view->with('ad',$ad);
		});
		// view()->composer('posts.show', function ($view) {
		// 	$ad=Ad::where('id',2)->first();
		// 	$view->with('ad',$ad);
		// });
		view()->composer('partials.elements.nav', function ($view) {
			$noti=[];
			if(Auth::check()){
				$noti=Notification::where('head_id',Auth::user()->id)->latest()->get()->take(4);
			}
		    $view->with('noti',$noti);
		});
		view()->composer('layouts.dashboard', function ($view) {
			$count['a']='b';
			if(Auth::user()->admin){
				$count['admins']=User::admin()->count();
				$count['editors']=User::editor()->count();
				$count['users']=User::user()->count();
			}
			$view->with('count',$count);
		});
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
