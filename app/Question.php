<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	public $timestamps = false;
	protected $fillabe=['content','post_id'];
	
    public function post(){
        return $this->belongsTo('App\Post');
    }
}
