<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
	public $timestamps = false;
    protected $fillable=['user_id','comment_id'];

    public function comment(){
        return $this->belongsTo('App\Comment');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
