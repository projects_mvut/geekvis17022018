<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrow extends Model
{
	public $timestamps = false;
    protected $fillable=['user_id','head_id'];
}
