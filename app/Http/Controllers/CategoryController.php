<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;



use Auth;
use App\Arrow;
use App\Message;
use App\User;
use App\Post;
use App\Comment;
use App\Like;
use App\Vote;
use Carbon\Carbon;
use Mail;
use Session;
use Input;
use App\Http\Requests\MessageRequest;


class CategoryController extends Controller
{
	public function __construct(){
		$this->middleware('admin', ['except' => ['index']]);
	}

	public function index(Category $category){
		$posts=$category->posts()->latest()->get();
		//$posts = $category->posts()->paginate(4);
		$posts = $category->posts()->published()->latest()->paginate(4);
		return view('posts.index',compact('posts'));
	}

	public function save(Request $request){
		$input=$request->all();
		if($input['title']){
			$val=str_slug($input['title']);
			if(Category::where('slug',$val)->count()){
				$val=$val.'-'.str_random(4);
			}
			$input['slug']=$val;
			Category::create($input);
		}
		return redirect()->back();
	}

	public function edit(Category $category){
		return view('pages.categoryEdit',compact('category'));
	}

	public function update(Category $category, Request $request){
		$input=$request->all();
		if($input['title']){
			$category->update(['title'=>$input['title']]);
		}
		return redirect('/');
	}

	public function delete(Category $category){
		$category->delete();
		return redirect('/');
	}
}
