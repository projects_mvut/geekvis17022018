<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Arrow;
use App\Message;
use App\User;
use App\Post;
use App\Category;
use App\Comment;
use App\Like;
use App\Vote;
use Carbon\Carbon;
use Mail;
use Session;
use Input;
use App\Http\Requests;
use App\Http\Requests\MessageRequest;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

	public function userJson(){
		$users=User::lists('name','username')->toArray();
		$b=[];
		foreach($users as $username=>$name){
			array_push($b,['name'=>$name,'url'=>'/profile/'.$username]);
		}
		// dd($b);
		return response()->json($b);
	}

	public function verify(){
		return view('pages.verify');
	}

	public function next(Post $post){
		Session::set('nextPost',$post->slug);
		return redirect('login');
	}

	public function verifyToken($token){
		if($token){
			$user=User::where('token',$token)->first();
			$user->update(['token'=>null]);
			Mail::send('emails.welcome',['user'=>$user],
				function($m) use ($user){
					$m->to($user->email, $user->username)
					->subject('Welcome To '.env('SITE_NAME'));
				});
			return redirect('/')->with('status','Your Email have been verified. You can now login.');
		}
		return redirect('/')->with('status-alert','Invalid Token');
	}

	public function sendToken(Request $request){
		$input=$request->all();
		$email=$input['email'];
		if($email){
			$user=User::where('email',$email)->first();
			if($user->token){
				Mail::queue('emails.verify', ['user' => $user], function($message) use ($user){
					$message->to($user->email, $user->username)
					->subject(env('SITE_NAME').' | Verify Your email');
				});
				return redirect('/')->with('status','Email Sent');
			}
			else{
				return redirect('/')->with('status','Already Verified');
			}
		}
		return redirect('/')->with('status-alert','Email Not Registered');
	}

	public function about(){
		return view('pages.about');
	}

	public function privacy(){
		return view('pages.privacy');
	}

	public function contact(){
		return view('pages.contact');
	}

	public function uploadData(){
		return view('pages.uploadData');
	}

	public function setDate($unit){
		if(isset($unit['created'])){
			return Carbon::createFromTimestamp($unit['created']);
		}
		return Carbon::now();
	}

	public function storeData($data){
		foreach ($data['users'] as $unit) {
			$count=User::where('email',$unit['email'])->count();
			if(!$count){
				$unit['password']=bcrypt($unit['password']);
				$unit['created_at']=$this->setDate($unit);
				User::create($unit);
			}
		}
		foreach ($data['snakes'] as $unit) {
			if(User::where('id',$unit['head_id'])->count()){
				if(User::where('id',$unit['user_id'])->count()){
					Arrow::create($unit);
				}
			}
		}
		foreach ($data['categories'] as $unit) {
			$unit['slug']=str_slug($unit['title']);
			Category::create($unit);
		}
		foreach ($data['posts'] as $unit) {
			if(isset($unit['category_id'])){
				$ifcat=Category::where('id',$unit['category_id'])->count();
				if(!$ifcat){
					unset($unit['category_id']);
				}
			}
			$count=User::where('id',$unit['user_id'])->count();
			if($count){
				$unit['created_at']=$this->setDate($unit);
				Post::create($unit);
			}
		}
		foreach ($data['comments'] as $unit) {
			if(User::where('id',$unit['user_id'])->count()){
				if(Post::where('id',$unit['post_id'])->count()){
					$unit['created_at']=$this->setDate($unit);
					Comment::create($unit);
				}
			}
		}
		foreach ($data['likes'] as $unit) {
			if(User::where('id',$unit['user_id'])->count()){
				if(Post::where('id',$unit['post_id'])->count()){
					Like::create($unit);
				}
			}
		}
		foreach ($data['votes'] as $unit) {
			if(User::where('id',$unit['user_id'])->count()){
				if(Comment::where('id',$unit['comment_id'])->count()){
					Vote::create($unit);
				}
			}
		}
	}

	public function saveData(Request $request){
		$input=$request->all();
		if(env('ALLOW_UPLOAD_DATA')){
			if($input['key']=='bbgUploadDataFileeeeeeee'){
				$file_data=file($request->file('datafile'))[0];
				$data=json_decode($file_data,true);
				$this->storeData($data);
				return redirect()->back()->with('status','Done');
			}
		}
		return redirect()->back()->with('status-danger','Invalid');
	}


	public function index(){
		if(Auth::check()){
			$following=Arrow::where('user_id',Auth::user()->id)->get()->pluck('head_id')->toArray();
			array_push($following, Auth::user()->id);
			$posts=Post::whereIn('user_id',$following)->published()->latest()->get();
			$posts=Post::whereIn('user_id',$following)->published()->latest()->paginate(4);

			return view('posts.index',compact('posts'));
		}
		$latest_posts=Post::published()->latest()->take(3)->get();
		$popular_posts=Post::published()->orderBy('views','desc')->take(3)->get();
		$comment=Comment::latest()->first();
		$answered_post=[];	
		if($comment){
			$answered_post=$comment->post;
		}
		return view('pages.index',compact('latest_posts','popular_posts','answered_post'));
	}

	public function sendMessage(MessageRequest $request){
		$input=$request->all();
		Message::create($input);
		return redirect('/')->with('status','Message Sent');
	}


	public function search(){
		if (Input::has('query') && strlen(Input::get('query')) > 3) {
		
			$query = Input::get('query');
			$articles=Post::where('content', 'LIKE', "%$query%")->get();
			$articles=Post::where('content', 'LIKE', "%$query%")->latest()->paginate(5);
			return view('pages.search',compact('query','articles'));
			
			//$query = Input::get('query');
			//$articles_title=Post::where('title', 'LIKE', "%$query%")->published()->latest()->get();
			//$articles_content=Post::where('content', 'LIKE', "%$query%")->published()->latest()->get();	
			//$articles=$articles_content->merge($articles_title);
			//$articles=Post::published()->latest()->paginate(5);
			//return view('pages.search',compact('query','articles'));
		}
		else{
			return redirect('/');
		}
	}
}
