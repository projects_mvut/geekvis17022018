<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use App\User;
use App\Ad;
use App\Post;
use App\Message;
use App\Article;
use App\Discussion;

class DashboardController extends Controller
{
	public function __construct(){
		$this->middleware('admin', ['except' => ['index','user_posts','ads','save_ads']]);
		$this->middleware('auth', ['only' => ['index']]);
		$this->middleware('owner', ['only' => ['user_articles']]);
	}

	public function ads(){
		$ad=Ad::where('id',1)->first();
		$ad2=Ad::where('id',2)->first();
		return view('dashboard.ads',compact('ad','ad2'));
	}

	public function save_ad1(Request $request){
		$input=$request->all();
		if(!$input['title']){
			$input['title']=str_random(6);
		}
		include 'ImageHandler.php';
		$ad=Ad::where('id',1)->first();
		if($ad){
			$ad->update($input);
		}
		else{
			Ad::create($input);
		}
		return redirect()->back()->with('status','Ad Saved');
	}

	public function save_ad2(Request $request){
		$input=$request->all();
		if(!$input['title']){
			$input['title']=str_random(6);
		}
		include 'ImageHandler.php';
		$ad=Ad::where('id',2)->first();
		if($ad){
			$ad->update($input);
		}
		else{
			Ad::create($input);
		}
		return redirect()->back()->with('status','Ad Saved');
	}

	public function index(){
		if(Auth::user()->admin){
			return redirect('dashboard/users');
		}
		return redirect('/');
	}

	public function email(){
		$posts=Post::latest()->get()->take(48);
		return view('dashboard.email',compact('posts'));
	}

	public function send_email(Request $request){
		$input=$request->all();
		$posts=$input['posts'];
		$posts=rtrim($posts, ",");
		if($posts){
			$posts=explode(",", $posts);
			$posts=Post::whereIn('id', $posts)->get();
			if (array_key_exists('all', $input)) {
				$users=User::all();
			}
			else{
				$users=User::where('admin',true)->get();
			}
			// dd($users->toArray());
			$url=env('SITE_URL');
			foreach ($users as $user) {
				Mail::queue('emails.mass',['posts'=>$posts,'url'=>$url],
					function($m) use ($user){
						$m->to($user->email, $user->name)
						->subject('GeekVis | Recommended questions for practice');
					});
				// Mail::queue('emails.mass',['posts'=>$posts,'url'=>$url],
				// 	function($m) use ($user){
				// 		$m->to('bharatbgarg4@gmail.com', 'Bharat Garg')
				// 		->subject('GeekVis | Recommended questions for practice');
				// 	});
			}
			return redirect()->back()->with('status','Emails Sent');
		}
		return redirect()->back()->with('status-alert','Select Posts');
	}

	public function users(){
		$users=User::user()->get();
		return view('dashboard.users',compact('users'));
	}

	public function editors(){
		$users=User::editor()->get();
		return view('dashboard.editors',compact('users'));
	}

	public function admins(){
		$users=User::admin()->get();
		return view('dashboard.admins',compact('users'));
	}

	public function user_delete(User $user){
		if($user->admin){
			return redirect()->back()->with('status-alert','Admin can not be Deleted');
		}
		$user->delete();
		return redirect()->back()->with('status-danger','User Deleted');
	}

	public function makeEditor(User $user){
		$user->update(['editor'=>true]);
		return redirect('dashboard/editors')->with('status','User Assigned Editor');
	}

	public function makeUser(User $user){
		$user->update(['editor'=>false]);
		return redirect('dashboard/users')->with('status-alert','Editor unassigned to User');
	}

	public function messages(){
		$messages=Message::latest()->get();
		return view('dashboard.messages',compact('messages'));
	}

	public function delete_message($message){
		Message::where('id',$message)->firstOrFail()->delete();
		return redirect()->back()->with('status-danger','Message Deleted');
	}

	public function user_posts(User $user){
		$posts=$user->posts()->latest()->get();
		return view('dashboard.posts',compact('posts'));
	}

	public function properify(){
		$users=User::all();
		foreach($users as $user){
			$b['username']=str_slug($user->username);
			if(!$user->imgUrl){
				$b['imgUrl']='/images/default.png';
			}
			$user->update($b);
		}
		return redirect('/');
	}
}