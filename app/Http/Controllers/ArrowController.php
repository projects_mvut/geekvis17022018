<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Arrow;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArrowController extends Controller
{
    public function __construct(){
    	$this->middleware('confirmed', ['only' => ['follow','unfollow']]);
    }

    public function follow(User $user){
    	$count=Arrow::where('user_id',Auth::user()->id)->where('head_id',$user->id)->count();
    	if($count){
	    	return redirect()->back()->with('status-alert','Already Following');
    	}
    	Arrow::create(['user_id'=>Auth::user()->id,'head_id'=>$user->id]);
    	return redirect()->back();
    }

    public function unfollow(User $user){
    	Arrow::where('user_id',Auth::user()->id)->where('head_id',$user->id)->delete();
    	return redirect()->back();
    }
}
