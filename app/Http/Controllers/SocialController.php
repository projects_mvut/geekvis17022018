<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Arrow;
use Auth;
use Mail;

class SocialController extends Controller
{
	public function redirectToSocial($social)
	{
		return Socialite::driver($social)->redirect();
	}

	
	public function handleSocialCallback(Request $request)
	{
		$social=$request->path();
		$user = Socialite::driver($social)->user();
		$email=$user->getEmail();
		$logu=User::where('email',$email)->first();
		if(!is_null($logu)){
			Auth::login($logu);
		}
		else{
			$name=$user->getName();
			if(!$name){
				$name=$email;
			}
			$username=str_slug($name).'-'.str_random(8);
			$data=['name'=>$name,'email'=>$email,'password'=>'q','username'=>$username];
			$user=User::create($data);
			$himanshu=User::where('email','himanshu.vasistha@gmail.com')->first();
			if($himanshu){
				Arrow::create(['user_id'=>$user->id,'head_id'=>$himanshu->id]);
				Arrow::create(['head_id'=>$user->id,'user_id'=>$himanshu->id]);
			}
			Mail::queue('emails.notify',['user'=>$user],
				function($m){
					$m->to('bharatbgarg4@gmail.com','Bharat Garg')
					->cc('himanshu.vasistha@gmail.com','Himanshu')
					->subject(env('SITE_NAME').' New User Registered');
				});
			Mail::queue('emails.welcome',['user'=>$user],
				function($m) use ($user){
					$m->to($user->email, $user->name)
					->subject('Welcome To '.env('SITE_NAME'));
				});
			Auth::login($user);
		}
		if($request->session()->has('nextPost')){
			return redirect('/post/'.$request->session()->pull('nextPost'));
		}
		return redirect('/');
	}
}
