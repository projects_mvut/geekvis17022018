<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use App\Like;
use App\Category;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
class PostController extends Controller
{

	public function __construct(){
		$this->middleware('admin', ['only' => ['boost','unboost']]);
		$this->middleware('owner', ['only' => ['edit','update','delete']]);
		$this->middleware('auth', ['only' => ['create','store']]);
	}

	public function rss(){
		header('Content-Type: application/xml; charset=UTF-8');
		$xml = new \XMLWriter();
		$xml->openMemory();
		$xml->startDocument('1.0', 'UTF-8');
		$xml->startElement('articles');
		$posts=Post::published()->latest()->take(6)->get();
		foreach($posts as $post) {
			$xml->startElement('data');
			$xml->writeAttribute('id', $post->id);
			$xml->writeAttribute('title', $post->title);
			$xml->writeAttribute('content', $post->content);
			$xml->endElement();
		}
		$xml->endElement();
		$xml->endDocument();

		$content = $xml->outputMemory();
		$xml = null;

		return response($content)->header('Content-Type', 'text/xml');
	}

	public function checkOwner($post){
		if(Auth::check()){
			if(Auth::user()->admin){
				return 1;
			}
			if(Auth::user()->id==$post->user_id){
				return 1;
			}
		}
		return 0;
	}

	public function checkLiked($post){
		if(Auth::check()){
			return Like::where('user_id',Auth::user()->id)->where('post_id',$post->id)->count();
		}
		return 0;
	}

	public function boost(Post $post){
		$post->update(['boost'=>1]);
		return redirect()->back()->with('status','Post Boosted');
	}

	public function unboost(Post $post){
		$post->update(['boost'=>0]);
		return redirect()->back()->with('status-alert','Post Unboosted');
	}
	protected $posts_per_page = 2;

	public function index(Request $request){
		$posts = Post::paginate($this->posts_per_page);
		if($request->ajax()) {
			return [
			'posts' => view('posts.data')->with(compact('posts'))->render(),
			'next_page' => $posts->nextPageUrl()
			];
		}
/*		$posts=Post::published()->latest()->get();
*/		return view('posts.index',compact('posts'));
	}
	public function fetchNextPostsSet($page) {



	}
	public function show(Post $post){
		$comments=$post->comments()->latest()->get();
		$comments=$post->comments()->latest()->paginate(2);

		$post->update(['views'=>$post->views+1]);
		$owner=$this->checkOwner($post);
		$liked=$this->checkLiked($post);
		$title=$post->title;
		$keywords=$post->keywords;
		$description=$post->description;
		return view('posts.show',compact('post','comments','owner','liked','title','keywords','description'));
	}

	public function embed(Post $post){
		return view('posts.embed',compact('post'));

	}

	public function create(){
		$select_category=Category::lists('title', 'id')->toArray();
		$categories=Category::all();
		return view('posts.create',compact('categories','select_category'));
	}

	public function edit(Post $post){
		$select_category=Category::lists('title', 'id')->toArray();
		$categories=Category::all();
		return view('posts.edit',compact('post','categories','select_category'));
	}

	public function getSlug($input){
		$slug=str_slug($input['title']);
		if(Post::where('slug',$slug)->count()){
			$slug=$slug.'-'.str_random(8);
		}
		return $slug;
	}

	public function store(PostRequest $request){
		$input=$request->all();
		if(Auth::user()->admin){
			if(Post::where('slug',$input['slug'])->count()){
				$input['slug']=$this->getSlug($input);
			}
		}
		else{
			$input['slug']=$this->getSlug($input);
		}
		include 'ImageHandler.php';
		$input['user_id']=Auth::user()->id;
		$input['content']=str_replace('"uploads/images/','"/uploads/images/',$input['content']);
		$input['published']=true;
		if($input['submit']=="Draft"){
			$input['published']=false;
		}
		Post::create($input);
		return redirect('/')->with('status','Post Created');
	}
	public function update(Post $post, PostRequest $request){
		$input=$request->all();
		if(Auth::user()->admin){
			if($input['slug']!=$post->slug){
				$count=Post::where('slug',$input['slug'])->count();
				if($count){
					$input['slug']=$this->getSlug($input);
				}
			}
		}
		include 'ImageHandler.php';
		$input['published']=true;
		$god=0;
		if($input['submit']=="Draft"){
			$input['published']=false;
			$god=1;
		}
		$post->update($input);
		if($god){
			return redirect('post/'.$post->slug.'/edit')->with('status','Post Saved');
		}
		return redirect('post/'.$post->slug)->with('status','Post Updated');
	}

	public function delete(Post $post){
		$post->delete();
		return redirect('/')->with('status-danger','Post Deleted');
	}
}
