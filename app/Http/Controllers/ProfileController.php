<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Arrow;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

	public function __construct(){
		$this->middleware('admin', ['only' => ['create']]);
		$this->middleware('confirmed', ['only' => ['show']]);
		$this->middleware('owner', ['except' => ['create','wall']]);
	}

	public function wall(User $user){
		$followers=Arrow::where('head_id',$user->id)->get()->pluck('user_id')->toArray();
		$following=Arrow::where('user_id',$user->id)->get()->pluck('head_id')->toArray();
		$followers=User::whereIn('id',$followers)->get();
		$following=User::whereIn('id',$following)->get();
		$follow=0;
		$editors=User::where('editor',true)->get();
		if(Auth::check()){
			$follow=Arrow::where('user_id',Auth::user()->id)->where('head_id',$user->id)->count();
		}
		$posts=Post::where('user_id',$user->id)->published()->latest()->get();
		$posts=Post::where('user_id',$user->id)->published()->latest()->paginate(4);

		$drafted=0;
		if(Auth::check()){
			if(Auth::user()->admin){
				$drafted=Post::where('user_id',$user->id)->drafted()->latest()->get();
			}
		}
		return view('profile.index',compact('user','followers','following','follow','editors','posts','drafted'));
	}

	public function create(){
		return view('dashboard.create_user');
	}

	public function store(UserCreateRequest $request){
		$input=$request->all();
		User::create($input);
		return redirect('dashboard/users')->with('status','User Created');
	}

	public function show(User $user){
		return view('dashboard.profile',compact('user'));
	}

	public function update(ProfileRequest $request,User $user){
		$input=$request->all();
		if($request->hasFile('imgUrl')){
			$imageName = Auth::user()->username.'-'.str_random(8).'.'.$request->file('imgUrl')->getClientOriginalExtension();
			$request->file('imgUrl')->move(
				base_path() . '/public/uploads/profile/',$imageName);
			$input['imgUrl']='/uploads/profile/'.$imageName;

			$user->update([
				'name' => $input['name'],
				'username' => $input['username'],
				'imgUrl'=>$input['imgUrl'],
				'bio'=>$input['bio'],
				]);
		}
		else{
			$user->update([
				'name' => $input['name'],
				'username' => $input['username'],
				'bio'=>$input['bio'],
				]);
		}
		return redirect('dashboard/'.$user->username.'/profile')->with('status','Profile Updated');
	}

	public function password(ChangePasswordRequest $request,User $user){
		$input=$request->all();
		$user->update(['password'=>bcrypt($input['password'])]);
		return redirect()->back()->with('status','Password Updated');
	}
}
