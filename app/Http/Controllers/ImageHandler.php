<?php
if($request->hasFile('imgUrl')){
	$imageName=str_slug($input['title']).'-'.str_random(8);
	$imageName=$imageName.'.'.$request->file('imgUrl')->getClientOriginalExtension();
	$request->file('imgUrl')->move(base_path() . '/public/uploads/images/',$imageName);
	$input['imgUrl']='/uploads/images/'.$imageName;
}
if($request->hasFile('boostUrl')){
	$imageName=str_slug($input['title']).'-'.str_random(8);
	$imageName=$imageName.'.'.$request->file('boostUrl')->getClientOriginalExtension();
	$request->file('boostUrl')->move(base_path() . '/public/uploads/images/',$imageName);
	$input['boostUrl']='/uploads/images/'.$imageName;
}

if($request->hasFile('csvUrl')){
	$questions = array_map('str_getcsv', file($request->file('csvUrl')));
	$head=$questions[0];
	$size=sizeof($head);
	$input['content']=$input['content'].'<hr>';
	$count=1;
	foreach($questions as $question){
		if($question[0]!='question_number'){
			if($question[6]!='fil'){
				$q='<div class="csvQues">';
				$q=$q.'<h5 class="qno"> Question '.$count.'</h5>';
				$q=$q.'<p class="qqu">'.$question[2].'</p>';
				if($question[7]){
					$q=$q.'<div class="qop"> <span class="qal"> A </span>'.$question[7].'</div>';
				}
				if($question[8]){
					$q=$q.'<div class="qop"> <span class="qal"> B </span>'.$question[8].'</div>';
				}
				if($question[9]){
					$q=$q.'<div class="qop"> <span class="qal"> C </span>'.$question[9].'</div>';
				}
				if($question[10]){
					$q=$q.'<div class="qop"> <span class="qal"> D </span>'.$question[10].'</div>';
				}
				$q=$q.'</div><hr>';
				$input['content']=$input['content'].$q;
				$count=$count+1;
			}
		}
	}
}