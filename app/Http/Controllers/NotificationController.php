<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Notification;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $notifications=Notification::where('head_id',Auth::user()->id)->latest()->get();
        return view('pages.bell',compact('notifications'));
    }
}
