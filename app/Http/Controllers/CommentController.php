<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Controllers\Controller;
use App\Events\StarEvent;
use App\Comment;
use App\Post;
use App\Notification;
use App\Like;
use App\Vote;
use App\User;
use Auth;
use Mail;

class CommentController extends Controller
{
	public function __construct(){
		$this->middleware('confirmed', ['only' => ['save','like','unlike']]);
		$this->middleware('owner', ['only' => ['delete']]);
	}

	public function unstar(User $user){
		Auth::user()->update(['star'=>0]);
		return 'unstarred';
	}

	public function vote(Comment $comment){
		$count=Vote::where('user_id',Auth::user()->id)->where('comment_id',$comment->id)->count();
		if($count){
			return redirect()->back()->with('status-alert','Already Liked');
		}
		$comment->votes()->create([
			'user_id' => Auth::user()->id,
			]);

		return redirect()->back();
	}

	public function unvote(Comment $comment){
		Vote::where('user_id',Auth::user()->id)->where('comment_id',$comment->id)->delete();
		return redirect()->back();
	}


	public function save(Post $post, CommentRequest $request){
		$input=$request->all();
		$input['title']='comment';
		include 'ImageHandler.php';
		$input['post_id']=$post->id;
		$input['user_id']=Auth::user()->id;
		$input['email']=Auth::user()->email;
		Comment::create($input);
		$noti=['type'=>1,'post_id'=>$post->id,'head_id'=>$post->user->id,'user_id'=>Auth::user()->id];
		Notification::create($noti);
		event(new StarEvent($post->user->username));
		$post->user()->update(['star'=>1]);
		Mail::raw('You commented on geekvis', function ($message) {
   		$message->to(Auth::user()->email);
});
		return redirect()->back();
	}

	public function delete(Post $post, Comment $comment){
		$comment->delete();
		return redirect()->back();
	}

	public function like(Post $post){
		$count=Like::where('user_id',Auth::user()->id)->where('post_id',$post->id)->count();
		if($count){
			return redirect()->back()->with('status-alert','Already Liked');
		}
		$post->likes()->create([
			'user_id' => Auth::user()->id,
			]);
		$noti=['type'=>0,'post_id'=>$post->id,'head_id'=>$post->user->id,'user_id'=>Auth::user()->id];
		Notification::create($noti);
		event(new StarEvent($post->user->username));
		$post->user()->update(['star'=>1]);
		return redirect()->back();
	}

	public function unlike(Post $post){
		Like::where('user_id',Auth::user()->id)->where('post_id',$post->id)->delete();
		return redirect()->back();
	}
}
