process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');

// './node_modules/socket.io-client/socket.io.min.js',

elixir(function(mix) {
    mix.sass('app.scss')
    .scripts([
    	'./vendor/bower_components/jquery/dist/jquery.min.js',
    	'./vendor/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    	],
    	'resources/assets/js/vendor.js')
    .scripts(['vendor.js','main.js'])
    .version(['css/app.css','js/all.js']);
});